import React from "react";
import { useState } from "react";
import FormCheckbox from "./FormCheckbox";
import FormInput from "./FormInput";
import FormSelect from "./FormSelect";

const Form = () => {
  const [values, setValues] = useState({
    firstname: "",
    secondname: "",
    age: "",
    isEmployed: false,
  });

//   const handleSubmit = (e) => {
//     e.preventDefault;
//   };

  const inputs = [
    {
      id: 1,
      name: "firstname",
      type: "text",
      placeholder: "First Name",
      label: "Firstname",
    },
    {
      id: 2,
      name: "lastname",
      type: "text",
      placeholder: "Last Name",
      label: "Lastname",
    },
    {
      id: 3,
      name: "age",
      type: "text",
      placeholder: "Age",
      label: "Age",
    },
    {
      id: 4,
      name: "isemployed",
      type: "checkbox",
      placeholder: null,
      label: "Employed",
    }
  ];

  return (
    <form className="form">
      {inputs.map((input) => {
        //return <FormInput key={input.id} name={input.name} type={input.type} placeholder={input.placeholder} label={input.label} />;
        return <FormInput key={ input.id } { ...input } />
      })}

      <FormSelect />
      <FormCheckbox type="checkbox" name="sauces"/>
    </form>
  );
};

export default Form;
