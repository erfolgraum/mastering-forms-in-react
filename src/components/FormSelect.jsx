import React from "react";

const FormSelect = () => {
  return (
    <div className="input-box">
      <label htmlFor="selectColor">Favourite Color</label>
      <select name="selectColor">
        <option value="" disabled></option>
        <option value="red">red</option>
        <option value="green">green</option>
        <option value="blue">blue</option>
      </select>
    </div>
  );
};

export default FormSelect;
