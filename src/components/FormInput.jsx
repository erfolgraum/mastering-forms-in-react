import React from "react";

const FormInput = (props) => {

  return (
    <>
      <div className="input-box">
        <label htmlFor={props.name}>{props.label}</label>
        <input type={props.type} 
                name={props.name}
                placeholder={props.placeholder} 
             />
      </div>

      {/* <div className="input-box">
        <label htmlFor="isEmployed">Employed</label>
        <input type="checkbox" name="isemployed" />
      </div>

      <div className="input-box">
        <label htmlFor="selectColor">Favourite Color</label>
        <select name="selectColor">
          <option value="" disabled></option>
          <option value="red">red</option>
          <option value="green">green</option>
          <option value="blue">blue</option>
        </select>
      </div>

      <div className="input-box">
        <label htmlFor="sauces">Sauces</label>
        <input type="checkbox" name="sauces" />
        <input type="checkbox" name="sauces" />
      </div>

      <div className="input-box radio-box">
        <label htmlFor="bestRadio">Best Stooge</label>
        <div>
          <input type="radio" name="bestRadio" value="Larry" />
          Larry
          <input type="radio" name="bestRadio" value="Larry" />
          Moe
          <input type="radio" name="bestRadio" value="Larry" />
          Curly
        </div>
      </div>

      <div className="input-box">
        <textarea
          name="notes"
          placeholder="Notes"
          cols="40"
          rows="2"
        ></textarea>
      </div> */}
    </>
  );
};

export default FormInput;
