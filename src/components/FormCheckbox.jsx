import React from "react";

const FormCheckbox = (props) => {
  const souses = ["Ketchup", "Mustard", "Mayonnaise", "Guacamole"];

  return (
    <div className="input-box input-box--sauce">
      <label className="sauceLabel" htmlFor={props.name}>
        Sauces
      </label>
      <div className="sauceInputBox">
        {souses.map((souse, index) => {
          return (
            <label key={index}>
              <input
                type={props.type}
                name={props.name}
                value={souse}
                className="sauceInput"
              />
              {souse}
            </label>
          );
        })}
      </div>
    </div>
  );
};

export default FormCheckbox;
